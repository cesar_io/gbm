package com.gbm.cesaraguirre.gbm.data.mapper;

import com.gbm.cesaraguirre.gbm.domain.entity.GraphPoint;
import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static org.mockito.Mockito.when;

public class IPCStateToGraph2DTest {

    public static double PRICE = 10f;

    private IPCState ipcStateNull;
    private @Mock IPCState mockIPCState;
    private Date date;
    private IPCStateToGraph2D mapper = new IPCStateToGraph2D();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        date = new Date();
        ipcStateNull = null;

        when(mockIPCState.getDate()).thenReturn(date);
        when(mockIPCState.getPrice()).thenReturn(PRICE);
    }


    @Test
    public void testMapWithNullInput() {
        Assert.assertNull(mapper.transform(ipcStateNull));
    }

    @Test
    public void testMap() {
        GraphPoint graphPoint = mapper.transform(mockIPCState);
        Assert.assertNotNull(graphPoint);
        Assert.assertEquals(((long)graphPoint.getX()), date.getTime());
        Assert.assertEquals(graphPoint.getY(), mockIPCState.getPrice());
    }
}
