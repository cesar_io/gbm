package com.gbm.cesaraguirre.gbm.domain.entity;

import android.support.annotation.NonNull;

import com.gbm.cesaraguirre.gbm.domain.interactors.GraphLabelFormater;

import java.util.List;

public class Graph2D {

    @NonNull private final List<GraphPoint> graphPointList;
    @GraphParameters.XScale final int xScale;
    @GraphParameters.YScale final int yScale;
    @GraphParameters.GridType final int gridType;
    @GraphParameters.InterpolationType final int interpolationType;
    @GraphParameters.PointType final int pointType;
    final int xAxisColor;
    final int yAxisColor;
    final int pointColor;
    final int interpolationLinesColor;
    final int gridLinesColor;
    final double xGridResolution;
    final double yGridResolution;
    final GraphLabelFormater gridXLabelFormater;
    final GraphLabelFormater gridYLabelFormater;


    public Graph2D(@NonNull List<GraphPoint> graphPointList,
                   @GraphParameters.XScale final int xScale,
                   @GraphParameters.YScale final int yScale,
                   @GraphParameters.GridType final int gridType,
                   @GraphParameters.InterpolationType final int interpolationType,
                   @GraphParameters.PointType final int pointType,
                   int xAxisColor,
                   int yAxisColor, int pointColor, int interpolationLinesColor, int gridLinesColor, double xGridResolution, double yGridResolution,
                   GraphLabelFormater gridXLabelFormater, GraphLabelFormater gridYLabelFormater) {
        this.graphPointList = graphPointList;
        this.yScale = yScale;
        this.xScale = xScale;
        this.gridType = gridType;
        this.interpolationType = interpolationType;
        this.pointType = pointType;
        this.xAxisColor = xAxisColor;
        this.yAxisColor = yAxisColor;
        this.pointColor = pointColor;
        this.interpolationLinesColor = interpolationLinesColor;
        this.gridLinesColor = gridLinesColor;
        this.xGridResolution = xGridResolution;
        this.yGridResolution = yGridResolution;
        this.gridXLabelFormater = gridXLabelFormater;
        this.gridYLabelFormater = gridYLabelFormater;
    }

    @NonNull
    public List<GraphPoint> getGraphPointList() {
        return graphPointList;
    }

    public int getyScale() {
        return yScale;
    }

    public int getxScale() {
        return xScale;
    }

    public int getGridType() {
        return gridType;
    }

    public @GraphParameters.InterpolationType int getInterpolationType() {
        return interpolationType;
    }

    public @GraphParameters.PointType int getPointType() {
        return pointType;
    }

    public int getxAxisColor() {
        return xAxisColor;
    }

    public int getyAxisColor() {
        return yAxisColor;
    }

    public int getPointColor() {
        return pointColor;
    }

    public int getInterpolationLinesColor() {
        return interpolationLinesColor;
    }

    public int getGridLinesColor() {
        return gridLinesColor;
    }

    public double getxGridResolution() {
        return xGridResolution;
    }

    public double getyGridResolution() {
        return yGridResolution;
    }

    public GraphLabelFormater getGridXLabelFormater() {
        return gridXLabelFormater;
    }

    public GraphLabelFormater getGridYLabelFormater() {
        return gridYLabelFormater;
    }

    public static Graph2DBuildable clone(Graph2D input) {
        return new Graph2DBuildable(input.getGraphPointList(), input.getxScale(), input.getyScale(),
                input.getGridType(), input.getInterpolationType(), input.getPointType(), input.getxAxisColor(), input.getyAxisColor(),
                input.getPointColor(), input.getInterpolationLinesColor(), input.getGridLinesColor(), input.getxGridResolution(), input.getyGridResolution(),
                input.gridXLabelFormater, input.gridYLabelFormater
                );
    }


    public static class Graph2DBuildable {

        List<GraphPoint> graphPointList;
        int yScale;
        int xScale;
        int gridType;
        int interpolationType;
        int pointType;
        int xAxisColor;
        int yAxisColor;
        int pointColor;
        int interpolationLinesColor;
        int gridLinesColor;
        double xGridResolution;
        double yGridResolution;
        GraphLabelFormater gridXLabelFormater;
        GraphLabelFormater gridYLabelFormater;


        public Graph2DBuildable(List<GraphPoint> graphPointList, int xScale, int yScale, int gridType, int interpolationType, int pointType, int xAxisColor, int yAxisColor, int pointColor, int interpolationLinesColor, int gridLinesColor, double xGridResolution, double yGridResolution,
                                GraphLabelFormater gridXLabelFormater, GraphLabelFormater gridYLabelFormater) {
            this.graphPointList = graphPointList;
            this.yScale = yScale;
            this.xScale = xScale;
            this.gridType = gridType;
            this.interpolationType = interpolationType;
            this.pointType = pointType;
            this.xAxisColor = xAxisColor;
            this.yAxisColor = yAxisColor;
            this.pointColor = pointColor;
            this.interpolationLinesColor = interpolationLinesColor;
            this.gridLinesColor = gridLinesColor;
            this.xGridResolution = xGridResolution;
            this.yGridResolution = yGridResolution;
            this.gridXLabelFormater = gridXLabelFormater;
            this.gridYLabelFormater = gridYLabelFormater;
        }

        public Graph2DBuildable withGraphPointList(List<GraphPoint> graphPoints) {
            this.graphPointList = graphPoints;
            return this;
        }

        public Graph2DBuildable withYScale(@GraphParameters.YScale final int yScale) {
            this.yScale = yScale;
            return this;
        }

        public Graph2DBuildable withXScale(@GraphParameters.XScale final int xScale) {
            this.xScale = xScale;
            return this;
        }

        public Graph2DBuildable withGridXResolution(double gridXResolution ) {
            this.xGridResolution = gridXResolution;
            return this;
        }

        public Graph2DBuildable withGridYResolution(double gridYResolution) {
            this.yGridResolution = gridYResolution;
            return this;
        }

        public Graph2DBuildable withGridType(@GraphParameters.GridType int gridType) {
            this.gridType = gridType;
            return this;
        }

        public Graph2DBuildable withPointType(@GraphParameters.PointType int pointType) {
            this.pointType = pointType;
            return this;
        }

        public Graph2DBuildable withInterpolationType(@GraphParameters.InterpolationType int interpolationType){
            this.interpolationType = interpolationType;
            return this;
        }

        public Graph2D build() {
            return new Graph2D(graphPointList, xScale,yScale, gridType, interpolationType,
                    pointType, xAxisColor, yAxisColor, pointColor, interpolationLinesColor, gridLinesColor,
                    xGridResolution, yGridResolution,
                    gridXLabelFormater, gridYLabelFormater);
        }
    }

}
