package com.gbm.cesaraguirre.gbm.presentation.view.custom;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.SurfaceHolder;

import com.gbm.cesaraguirre.gbm.domain.entity.Graph2D;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphParameters;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphPoint;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphViewPort;
import com.gbm.cesaraguirre.gbm.presentation.view.entity.GraphFragmentViewEntity;

public class PlotRenderer implements Runnable{
    private final SurfaceHolder surfaceHolder;
    //private Graph2D graphModel;

    private Paint circlePaint;
    private Paint interpolationPaint;
    private Paint gridPaint;
    private Paint xAxisPaint;
    private Paint yAxisPaint;
    private Rect surfaceRect;
    private GraphFragmentViewEntity modelToRender;

    //private GraphViewPort viewPort;

    private static final float GRID_TEXT_SIZE = 22f;


    public PlotRenderer(SurfaceHolder surfaceHolder) {
        this.surfaceHolder = surfaceHolder;
        circlePaint = new Paint();
        gridPaint = new Paint();
        gridPaint.setTextSize(GRID_TEXT_SIZE);
        circlePaint.setColor(Color.RED);
        circlePaint.setStyle(Paint.Style.FILL);
        xAxisPaint = new Paint();
        yAxisPaint = new Paint();
        interpolationPaint = new Paint();
        interpolationPaint.setStrokeWidth(1f);
    }

    /*
    public void setViewPort(GraphViewPort viewPort) {
        this.viewPort = viewPort;
    }*/

    /*
    synchronized public void setGraphModel(Graph2D graphModel) {
        this.graphModel = graphModel;
    }*/

    synchronized public void refreshSurfaceMeasures(int newWidth, int newHeight) {
        surfaceRect = new Rect(0, newHeight, newWidth, 0);
        run();
    }

    @Override
    public void run() {
        Canvas canvas = surfaceHolder.lockCanvas();
        synchronized (surfaceHolder) {
            doDraw(canvas);
        }
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    synchronized private void doDraw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);
        if (modelToRender == null || surfaceRect == null) {
            return;
        }

        xAxisPaint.setColor(modelToRender.getGraphToDisplay().getxAxisColor());
        yAxisPaint.setColor(modelToRender.getGraphToDisplay().getyAxisColor());

        RectF worldSectionToDisplay = new RectF(
                (float)(modelToRender.getBoxCenterX()-modelToRender.getBoxWidth()/2),
                (float)(modelToRender.getBoxCenterY()+modelToRender.getBoxHeight()/2),
                (float)(modelToRender.getBoxCenterX()+modelToRender.getBoxWidth()/2),
                (float)(modelToRender.getBoxCenterY()-modelToRender.getBoxHeight()/2)
        );

        GraphViewPort viewPort = new GraphViewPort(worldSectionToDisplay, surfaceRect);
        Graph2D graphModel = modelToRender.getGraphToDisplay();

        gridPaint.setColor(graphModel.getGridLinesColor());

        if (graphModel.getGridType() == GraphParameters.GRID_TYPE_X || graphModel.getGridType()==GraphParameters.GRID_TYPE_X_AND_Y) {
            //draw horizontal lines
            for (float dY = viewPort.getWorldSectionToDisplay().bottom; dY < viewPort.getWorldSectionToDisplay().top; dY += graphModel.getyGridResolution()) {
                GraphPoint p1 = viewPort.map(new GraphPoint(viewPort.getWorldSectionToDisplay().left, dY));
                GraphPoint p2 = viewPort.map(new GraphPoint(viewPort.getWorldSectionToDisplay().right, dY));
                canvas.drawLine((float) p1.getX(), (float) p1.getY(), (float) p2.getX(), (float) p2.getY(), gridPaint);
                canvas.drawText(graphModel.getGridYLabelFormater().execute(dY), (float) p1.getX(), (float) p1.getY() + 10, gridPaint);
            }
        }

        if (graphModel.getGridType() == GraphParameters.GRID_TYPE_Y || graphModel.getGridType()==GraphParameters.GRID_TYPE_X_AND_Y) {
            //draw vertical lines
            for (float dX = viewPort.getWorldSectionToDisplay().left; dX < viewPort.getWorldSectionToDisplay().right; dX += graphModel.getxGridResolution()) {
                GraphPoint p1 = viewPort.map(new GraphPoint(dX, viewPort.getWorldSectionToDisplay().bottom));
                GraphPoint p2 = viewPort.map(new GraphPoint(dX, viewPort.getWorldSectionToDisplay().top));
                canvas.drawLine((float) p1.getX(), (float) p1.getY(), (float) p2.getX(), (float) p2.getY(), gridPaint);
                canvas.drawText(graphModel.getGridXLabelFormater().execute(dX), (float) p1.getX(), (float) p1.getY() - 20, gridPaint);
            }
        }

        //Draw points
        GraphPoint lastDrawnPoint = null;
        for (GraphPoint point : graphModel.getGraphPointList()) {
            GraphPoint deviceCoords = viewPort.map(point);
            GraphPoint lastDrawnPointDeviceCoords = null;
            if (lastDrawnPoint!= null) {
               lastDrawnPointDeviceCoords = viewPort.map(lastDrawnPoint);
            }
            canvas.drawCircle((float)deviceCoords.getX(), (float)deviceCoords.getY(), 3, circlePaint);
            if (graphModel.getPointType() == GraphParameters.POINT_TYPE_BASE_POINT){
                canvas.drawLine((float)deviceCoords.getX(), (float)deviceCoords.getY(), (float)deviceCoords.getX(), (float)viewPort.getDeviceRect().top, interpolationPaint);
            }
            if (graphModel.getInterpolationType() == GraphParameters.INTERPOLATION_TYPE_RECT_LINES && lastDrawnPoint != null) {
                canvas.drawLine((float)deviceCoords.getX(), (float)deviceCoords.getY(), (float)lastDrawnPointDeviceCoords.getX(), (float)lastDrawnPointDeviceCoords.getY(), interpolationPaint);
            }
            lastDrawnPoint = point;
        }

        //axis drawing
        GraphPoint px1 = viewPort.map(new GraphPoint(-99999, 0));
        GraphPoint px2 = viewPort.map(new GraphPoint(99999, 0));
        canvas.drawLine((float)px1.getX(), (float)px1.getY(), (float)px2.getX(), (float)px2.getY(), xAxisPaint);
        GraphPoint py1 = viewPort.map(new GraphPoint(0, -99999));
        GraphPoint py2 = viewPort.map(new GraphPoint(0, 99999));
        canvas.drawLine((float)py1.getX(), (float)py1.getY(), (float)py2.getX(), (float)py2.getY(), yAxisPaint);

    }

    public synchronized void setModelToRender(GraphFragmentViewEntity graphFragmentViewEntity) {
        modelToRender = graphFragmentViewEntity;
        run();
    }

}
