package com.gbm.cesaraguirre.gbm.presentation.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import com.gbm.cesaraguirre.gbm.R
import com.gbm.cesaraguirre.gbm.presentation.view.adapter.AppSectionsPageAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var appSectionsPageAdapter: AppSectionsPageAdapter
    private lateinit var mViewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appSectionsPageAdapter = AppSectionsPageAdapter(supportFragmentManager)
        mViewPager = findViewById(R.id.pager)
        mViewPager.adapter = appSectionsPageAdapter

        /*
        var repository = IPCStateRepositoryImpl(
                IPCWebServiceResponseMapper((ResultObjResponseMapper())))

        var viewmodel = IPCListViewModel(GetLastIPCStatesUseCase(repository))
        */
    }
}
