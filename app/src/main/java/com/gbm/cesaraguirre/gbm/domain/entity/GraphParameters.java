package com.gbm.cesaraguirre.gbm.domain.entity;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class GraphParameters {

    public static final int Y_SCALE_LINEAR = 1;
    public static final int Y_SCALE_EXPONENTIAL = 2;
    @IntDef({Y_SCALE_LINEAR, Y_SCALE_EXPONENTIAL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface YScale {}

    public static final int X_SCALE_LINEAR = 1;
    public static final int X_SCALE_EXPONENTIAL = 2;
    @IntDef({X_SCALE_LINEAR, X_SCALE_EXPONENTIAL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface XScale {}

    public static final int GRID_TYPE_NO_GRID = 1;
    public static final int GRID_TYPE_X = 2;
    public static final int GRID_TYPE_Y = 3;
    public static final int GRID_TYPE_X_AND_Y = 4;
    @IntDef({GRID_TYPE_NO_GRID, GRID_TYPE_X, GRID_TYPE_Y, GRID_TYPE_X_AND_Y})
    @Retention(RetentionPolicy.SOURCE)
    public @interface GridType {}

    public static final int INTERPOLATION_TYPE_NO_INTERPOLATION = 1;
    public static final int INTERPOLATION_TYPE_RECT_LINES = 2;
    public static final int INTERPOLATION_TYPE_POLINOMIAL = 3;
    @IntDef({INTERPOLATION_TYPE_NO_INTERPOLATION, INTERPOLATION_TYPE_RECT_LINES, INTERPOLATION_TYPE_POLINOMIAL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface InterpolationType {}

    public static final int POINT_TYPE_INVISIBLE = 1;
    public static final int POINT_TYPE_CIRCLE = 2;
    public static final int POINT_TYPE_SQUARE = 3;
    public static final int POINT_TYPE_BASE_POINT = 4;
    @IntDef({POINT_TYPE_INVISIBLE, POINT_TYPE_CIRCLE, POINT_TYPE_SQUARE,POINT_TYPE_BASE_POINT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PointType {}

}
