package com.gbm.cesaraguirre.gbm.domain.interactors;

public class AxisYLabelFormater extends GraphLabelFormater {
    @Override
    public String execute(double input) {
        return input/1000 + "units";
    }
}
