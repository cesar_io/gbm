package com.gbm.cesaraguirre.gbm.presentation.view.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.gbm.cesaraguirre.gbm.R;
import com.gbm.cesaraguirre.gbm.data.mapper.IPCWebServiceResponseMapper;
import com.gbm.cesaraguirre.gbm.data.mapper.ResultObjResponseMapper;
import com.gbm.cesaraguirre.gbm.data.repository.IPCStateRepositoryImpl;
import com.gbm.cesaraguirre.gbm.domain.interactors.GetIPCStatesInRealTimeUseCase;
import com.gbm.cesaraguirre.gbm.domain.interactors.GetLastIPCStatesUseCase;
import com.gbm.cesaraguirre.gbm.presentation.view.custom.IPCGraphSurfaceView;
import com.gbm.cesaraguirre.gbm.presentation.view.entity.GraphFragmentViewEntity;
import com.gbm.cesaraguirre.gbm.presentation.viewmodel.IPCGraphViewModel;
import com.gbm.cesaraguirre.gbm.presentation.viewmodel.IPCRealTimeViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IPCGraphFragment extends Fragment {

    @BindView(R.id.progress) View progressView;
    IPCGraphSurfaceView surfaceView;
    private IPCGraphViewModel viewModel;

    public static IPCGraphFragment newInstance() {
        IPCGraphFragment fragment = new IPCGraphFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surfaceView = new IPCGraphSurfaceView(getContext());

        viewModel = ViewModelProviders.of(this).get(IPCGraphViewModel.class);
        viewModel.init(new GetLastIPCStatesUseCase(new IPCStateRepositoryImpl(new IPCWebServiceResponseMapper(new ResultObjResponseMapper()))));
        viewModel.getLiveDataForScreen().observe(this, this::refreshUI);
        viewModel.requestInitialState();
    }

    private void refreshUI(GraphFragmentViewEntity graphFragmentViewEntity) {
        if (graphFragmentViewEntity.getGraphToDisplay() != null) {
            surfaceView.refreshPlotModel(graphFragmentViewEntity);
        }
        if (graphFragmentViewEntity.isLoading()) {
            progressView.setVisibility(View.VISIBLE);
        } else {
            progressView.setVisibility(View.GONE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ipc_graph, container, false);
        FrameLayout frameArea = v.findViewById(R.id.graph_area);
        frameArea.addView(surfaceView);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        surfaceView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        surfaceView.resume();
    }

    @OnClick(R.id.moveLeft)
    public void moveLeftClick() {
        viewModel.moveLeft();
    }

    @OnClick(R.id.moveRight)
    public void moveRightClick() {
        viewModel.moveRight();
    }

    @OnClick(R.id.moveDown)
    public void moveDownClick() {
        viewModel.moveDown();
    }

    @OnClick(R.id.moveUp)
    public void moveUpClick() {
        viewModel.moveUp();
    }

    @OnClick(R.id.zoom_in_x)
    public void zoomInX() {
        viewModel.zoomXIn();
    }

    @OnClick(R.id.zoom_out_x)
    public void zoomOutX() {
        viewModel.zoomXOut();
    }

    @OnClick(R.id.zoom_in_y)
    public void zoomInY() {
        viewModel.zoomYIn();
    }

    @OnClick(R.id.zoom_out_y)
    public void zoomOutY() {
        viewModel.zoomYOut();
    }


    @OnClick(R.id.btn_point_type)
    public void clickBtnPointType() {
        viewModel.changeGridDisplayType();
    }

    @OnClick(R.id.btn_grid_mode)
    public void clickBtnGridMode() {
        viewModel.changeGridMode();
    }

    @OnClick(R.id.btn_grid_res)
    public void clickBtnGridRes() {
        viewModel.changeGridResolution();
    }
}
