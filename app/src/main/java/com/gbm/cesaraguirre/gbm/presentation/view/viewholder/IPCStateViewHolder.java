package com.gbm.cesaraguirre.gbm.presentation.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gbm.cesaraguirre.gbm.R;

public class IPCStateViewHolder extends RecyclerView.ViewHolder {

    TextView date;
    TextView price;

    public IPCStateViewHolder(View itemView) {
        super(itemView);
        date = itemView.findViewById(R.id.ipc_item_date_lbl);
        price = itemView.findViewById(R.id.ipc_item_price_lbl);
    }

    public TextView getDateTextView() {
        return date;
    }

    public TextView getPriceTextView() {
        return price;
    }
}
