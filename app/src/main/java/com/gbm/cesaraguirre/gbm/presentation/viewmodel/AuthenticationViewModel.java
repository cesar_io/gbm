package com.gbm.cesaraguirre.gbm.presentation.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.hardware.fingerprint.FingerprintManager;
import android.support.annotation.NonNull;

import com.gbm.cesaraguirre.gbm.presentation.view.entity.AuthViewEntity;
import com.multidots.fingerprintauth.FingerPrintAuthCallback;
import com.multidots.fingerprintauth.FingerPrintAuthHelper;

public class AuthenticationViewModel extends AndroidViewModel implements FingerPrintAuthCallback {

    private static final String HARDCODED_AUTH_CODE = "1234";
    private FingerPrintAuthHelper mFingerPrintAuthHelper;
    private MutableLiveData<AuthViewEntity> viewEntityLiveData;
    private MutableLiveData<Boolean> loginSuccessLiveData;

    public AuthenticationViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(){
        mFingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(getApplication(), this);
    }

    public LiveData<AuthViewEntity> getLiveDataForScreen() {
        if (viewEntityLiveData == null) {
            viewEntityLiveData = new MutableLiveData<>();
            viewEntityLiveData.postValue(new AuthViewEntity(true,
                    false,
                    null));
        }
        return viewEntityLiveData;
    }

    public LiveData<Boolean> getLoginSuccessLiveData() {
        if (loginSuccessLiveData==null) {
            loginSuccessLiveData = new MutableLiveData<>();
            //loginSuccessLiveData.postValue(false);
        }
        return loginSuccessLiveData;
    }

    public void startFingerPrintAuth() {
        viewEntityLiveData.postValue(new AuthViewEntity(false, true, null));
        mFingerPrintAuthHelper.startAuth();
    }

    public void startAuthWithPass() {
        viewEntityLiveData.postValue(
                new AuthViewEntity(false,
                        false,
                        new AuthViewEntity.LoginWithPassViewEntity(
                                "",
                                false)));
    }

    public void authWithCode(String code) {
        if (code==null || !code.equals(HARDCODED_AUTH_CODE)) {
            AuthViewEntity.LoginWithPassViewEntity errorMessageViewEntity;
            errorMessageViewEntity = new AuthViewEntity.LoginWithPassViewEntity("Codigo invalido", true);
            AuthViewEntity viewEntity = new AuthViewEntity(false, false, errorMessageViewEntity);
            viewEntityLiveData.postValue(viewEntity);
        } else {
            loginSuccessLiveData.postValue(true);
        }
    }

    public void tryAgainWithFingerprint() {
        mFingerPrintAuthHelper.startAuth();
        viewEntityLiveData.postValue(new AuthViewEntity(false, true, null));
    }

    @Override
    public void onNoFingerPrintHardwareFound() {
        AuthViewEntity.LoginWithPassViewEntity errorMessageViewEntity;
        errorMessageViewEntity = new AuthViewEntity.LoginWithPassViewEntity("No se encontro lector de huella", false);
        AuthViewEntity viewEntity = new AuthViewEntity(false, false, errorMessageViewEntity);
        viewEntityLiveData.postValue(viewEntity);
        mFingerPrintAuthHelper.stopAuth();
    }

    @Override
    public void onNoFingerPrintRegistered() {
        AuthViewEntity.LoginWithPassViewEntity errorMessageViewEntity;
        errorMessageViewEntity = new AuthViewEntity.LoginWithPassViewEntity("No se encontraron huellas registradas", false);
        AuthViewEntity viewEntity = new AuthViewEntity(false, false, errorMessageViewEntity);
        viewEntityLiveData.postValue(viewEntity);
        mFingerPrintAuthHelper.stopAuth();
    }

    @Override
    public void onBelowMarshmallow() {
        AuthViewEntity.LoginWithPassViewEntity errorMessageViewEntity;
        errorMessageViewEntity = new AuthViewEntity.LoginWithPassViewEntity("Tu SO no soporta lectura de huellas", false);
        AuthViewEntity viewEntity = new AuthViewEntity(false, false, errorMessageViewEntity);
        viewEntityLiveData.postValue(viewEntity);
        mFingerPrintAuthHelper.stopAuth();
    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        mFingerPrintAuthHelper.stopAuth();
        loginSuccessLiveData.postValue(true);
    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        AuthViewEntity.LoginWithPassViewEntity errorMessageViewEntity;
        errorMessageViewEntity = new AuthViewEntity.LoginWithPassViewEntity("Fallo al autenticar", true);
        AuthViewEntity viewEntity = new AuthViewEntity(false, false, errorMessageViewEntity);
        viewEntityLiveData.postValue(viewEntity);
        mFingerPrintAuthHelper.stopAuth();
    }

}
