package com.gbm.cesaraguirre.gbm.domain.interactors;

public abstract class GraphLabelFormater {

    public abstract String execute(double input);
}
