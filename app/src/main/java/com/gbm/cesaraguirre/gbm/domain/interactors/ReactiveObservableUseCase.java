package com.gbm.cesaraguirre.gbm.domain.interactors;

import io.reactivex.Observable;

public abstract class ReactiveObservableUseCase<T, A> {
    public abstract Observable<T> execute(A args);
}
