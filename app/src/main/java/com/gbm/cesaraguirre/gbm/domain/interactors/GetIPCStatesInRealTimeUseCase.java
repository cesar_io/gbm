package com.gbm.cesaraguirre.gbm.domain.interactors;

import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;
import com.gbm.cesaraguirre.gbm.domain.repository.IPCStateRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;

public class GetIPCStatesInRealTimeUseCase extends ReactiveObservableUseCase<List<IPCState>, GetIPCStatesInRealTimeUseCase.ExecuteArgs> {

    private final IPCStateRepository repository;

    public GetIPCStatesInRealTimeUseCase(IPCStateRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<List<IPCState>> execute(ExecuteArgs args) {
        Single<List<IPCState>> lastItems = repository.getIPCStatesSorted()
                .map(ipcStatesOrdered -> {
                    List<IPCState> lastIPCStates = new ArrayList<>();

                    //Get chunks of size specified in  args:ExecuteArgs
                    //each chunk has the last n items from the service response
                    for (int i = ipcStatesOrdered.size() - 1, c = 0; i >= 0 && c < args.getNumItems(); i--, c++) {
                        lastIPCStates.add(ipcStatesOrdered.get(i));
                    }
                    return lastIPCStates;
                });


        return Observable.interval(args.getRefreshIntervalInSeconds(), TimeUnit.SECONDS)
                .flatMap(aLong -> lastItems.toObservable());
    }

    public static class ExecuteArgs {
        final int refreshIntervalInSeconds;
        final int numItems;

        public ExecuteArgs(int refreshIntervalInSeconds, int numItems) {
            this.refreshIntervalInSeconds = refreshIntervalInSeconds;
            this.numItems = numItems;
        }

        public int getNumItems() {
            return numItems;
        }

        public int getRefreshIntervalInSeconds() {
            return refreshIntervalInSeconds;
        }
    }
}
