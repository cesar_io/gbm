package com.gbm.cesaraguirre.gbm.data.mapper;

import android.support.annotation.Nullable;

import com.gbm.cesaraguirre.gbm.data.response.IPCWebServiceResponse;
import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;

import java.util.Collection;
import java.util.List;

public class IPCWebServiceResponseMapper extends BaseDataMapper<Collection<IPCState>, IPCWebServiceResponse>{

    private final ResultObjResponseMapper resultObjResponseMapper;

    public IPCWebServiceResponseMapper(ResultObjResponseMapper mapper) {
        resultObjResponseMapper = mapper;
    }

    @Nullable
    @Override
    public List<IPCState> transform(IPCWebServiceResponse input) {
        return resultObjResponseMapper.transform(input.getResultObjList());
    }
}
