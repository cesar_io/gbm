package com.gbm.cesaraguirre.gbm.presentation.view.adapter;

import android.support.v7.util.DiffUtil;

import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;

public class IPCItemCallBack extends DiffUtil.ItemCallback<IPCState> {

    @Override
    public boolean areItemsTheSame(IPCState oldItem, IPCState newItem) {
        return oldItem.getDate().getTime() == newItem.getDate().getTime();
    }

    @Override
    public boolean areContentsTheSame(IPCState oldItem, IPCState newItem) {
        return oldItem.getDate().equals(newItem.getDate())
                && oldItem.getPrice().equals(newItem.getPrice());
    }
}
