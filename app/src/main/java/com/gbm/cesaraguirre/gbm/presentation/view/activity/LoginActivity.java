package com.gbm.cesaraguirre.gbm.presentation.view.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gbm.cesaraguirre.gbm.R;
import com.gbm.cesaraguirre.gbm.presentation.view.entity.AuthViewEntity;
import com.gbm.cesaraguirre.gbm.presentation.viewmodel.AuthenticationViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    AuthenticationViewModel activityViewModel;
    @BindView(R.id.start_fingerprint_auth_section) View startFingerprintAuthSection;
    @BindView(R.id.startAuthWithPass) View startAuthWithPassBtn;
    @BindView(R.id.reading_fingerprint_section) View readingFingerPrintAuthSection;
    @BindView(R.id.error_authentication_section) View errorAuthenticationSection;
    @BindView(R.id.fingerprint_error_message) TextView authErrorMessage;
    @BindView(R.id.pass_code_text) EditText loginCodeTxt;
    @BindView(R.id.tryAgainWithFingerPrintButton) Button btnTryAgain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        activityViewModel = ViewModelProviders.of(this).get(AuthenticationViewModel.class);
        activityViewModel.init();

        activityViewModel.getLiveDataForScreen().observe(this, this::refreshScreen);
        activityViewModel.getLoginSuccessLiveData().observe(this, this::loginResult);
    }

    private void loginResult(Boolean loginResult) {
        if (loginResult) {
            getApplication().startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    private void refreshScreen(AuthViewEntity viewEntity) {
        if(viewEntity.showStartAuthScreen()) {
            startFingerprintAuthSection.setVisibility(View.VISIBLE);
            readingFingerPrintAuthSection.setVisibility(View.GONE);
            errorAuthenticationSection.setVisibility(View.GONE);
        } else if (viewEntity.getShowReadingFingerpritnMessage()) {
            startFingerprintAuthSection.setVisibility(View.GONE);
            readingFingerPrintAuthSection.setVisibility(View.VISIBLE);
            errorAuthenticationSection.setVisibility(View.GONE);
        } else {
            startFingerprintAuthSection.setVisibility(View.GONE);
            readingFingerPrintAuthSection.setVisibility(View.GONE);
            errorAuthenticationSection.setVisibility(View.VISIBLE);

            authErrorMessage.setText(viewEntity.getFingerPrintErrorMessage().getFingerPrintErrorMessage());
            if (viewEntity.getFingerPrintErrorMessage().getDisplayTryAgainButton()){
                btnTryAgain.setVisibility(View.VISIBLE);
            } else {
                btnTryAgain.setVisibility(View.GONE);
            }
        }
    }

    @OnClick(R.id.startAuthWithPass)
    public void onClickStartAuthWithPass() {
        activityViewModel.startAuthWithPass();
    }

    @OnClick(R.id.btnAuth)
    public void onClickBtnAuth() {
        activityViewModel.startFingerPrintAuth();
    }

    @OnClick(R.id.btnAuthWithCode)
    public void onClickBtnAuthWithCode() {
        activityViewModel.authWithCode(loginCodeTxt.getText().toString());
    }

    @OnClick(R.id.tryAgainWithFingerPrintButton)
    public void tryAgainWithFingerPrint() {
        activityViewModel.tryAgainWithFingerprint();
    }

}
