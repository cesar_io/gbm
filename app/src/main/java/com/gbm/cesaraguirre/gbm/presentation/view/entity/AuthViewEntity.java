package com.gbm.cesaraguirre.gbm.presentation.view.entity;

import android.support.annotation.Nullable;

public class AuthViewEntity {

    private final boolean showStartAuthScreen;
    private final boolean showReadingFingerpritnMessage;
    private final LoginWithPassViewEntity fingerPrintErrorMessage;

    public AuthViewEntity(boolean showStartAuthScreen, boolean showReadingFingerpritnMessage, @Nullable LoginWithPassViewEntity fingerPrintErrorMessage) {
        this.showStartAuthScreen = showStartAuthScreen;
        this.showReadingFingerpritnMessage = showReadingFingerpritnMessage;
        this.fingerPrintErrorMessage = fingerPrintErrorMessage;
    }

    @Nullable
    public LoginWithPassViewEntity getFingerPrintErrorMessage() {
        return fingerPrintErrorMessage;
    }

    public boolean showStartAuthScreen() {
        return showStartAuthScreen;
    }

    public boolean getShowReadingFingerpritnMessage() {
        return showReadingFingerpritnMessage;
    }

    public static class LoginWithPassViewEntity {
        private final String fingerPrintErrorMessage;
        private final boolean displayTryAgainButton;

        public LoginWithPassViewEntity(String fingerPrintErrorMessage, boolean displayTryAgainButton) {
            this.fingerPrintErrorMessage = fingerPrintErrorMessage;
            this.displayTryAgainButton = displayTryAgainButton;
        }

        public String getFingerPrintErrorMessage() {
            return fingerPrintErrorMessage;
        }

        public boolean getDisplayTryAgainButton() {
            return displayTryAgainButton;
        }
    }


}
