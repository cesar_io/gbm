package com.gbm.cesaraguirre.gbm.presentation.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gbm.cesaraguirre.gbm.R;
import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;
import com.gbm.cesaraguirre.gbm.presentation.view.viewholder.IPCStateViewHolder;

public class IPCStatesListAdapter extends ListAdapter<IPCState, IPCStateViewHolder> {


    public IPCStatesListAdapter(@NonNull DiffUtil.ItemCallback<IPCState> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public IPCStateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ipc_state, parent, false);
        return new IPCStateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IPCStateViewHolder holder, int position) {
        holder.getDateTextView().setText(getItem(position).getDate().toString());
        holder.getPriceTextView().setText(getItem(position).getPrice().toString());
    }

}
