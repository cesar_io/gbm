package com.gbm.cesaraguirre.gbm.data.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class BaseDataMapper<O, I> {

    @Nullable
    public abstract O transform(I input);

    @NonNull
    public List<O> transform(@Nullable Collection<I> input) {
        final List<O> output = new ArrayList<>();
        if (input == null) {
            return output;
        }

        for (I inputItem : input) {
            output.add(transform(inputItem));
        }
        return output;
    }

}
