package com.gbm.cesaraguirre.gbm.domain.utils;

import com.gbm.cesaraguirre.gbm.domain.entity.DataSetMaxMin;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphPoint;

import java.util.Collection;

public final class GraphPointUtils {

    public static DataSetMaxMin getDataSetMaxMin(Collection<GraphPoint> graphPoints) {
        Double minX = null, minY=null, maxX=null, maxY=null;
        for (GraphPoint graphPoint : graphPoints) {
            if (minX == null || graphPoint.getX()<minX) {
                minX = graphPoint.getX();
            }
            if (minY == null || graphPoint.getY()<minY) {
                minY = graphPoint.getY();
            }
            if (maxX == null || graphPoint.getX()>maxX) {
                maxX = graphPoint.getX();
            }
            if (maxY == null || graphPoint.getY()>maxY) {
                maxY = graphPoint.getY();
            }
        }
        return new DataSetMaxMin(maxX, minX, maxY, minY);
    }

}
