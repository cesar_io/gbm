package com.gbm.cesaraguirre.gbm.presentation.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gbm.cesaraguirre.gbm.presentation.view.custom.IPCGraphSurfaceView;
import com.gbm.cesaraguirre.gbm.presentation.view.fragment.IPCGraphFragment;
import com.gbm.cesaraguirre.gbm.presentation.view.fragment.IPCRealTimeFragment;

public class AppSectionsPageAdapter extends FragmentPagerAdapter {

    private static final int NUM_SCREENS = 2;
    private static final int FIRST_SCREEN = 0;
    private static final int SECOND_SCREEN = 1;

    public AppSectionsPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case FIRST_SCREEN:
                return IPCGraphFragment.newInstance();
            case SECOND_SCREEN:
                return IPCRealTimeFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_SCREENS;
    }
}
