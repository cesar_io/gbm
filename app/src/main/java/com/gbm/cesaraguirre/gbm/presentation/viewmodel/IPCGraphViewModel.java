package com.gbm.cesaraguirre.gbm.presentation.viewmodel;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.graphics.Color;

import com.gbm.cesaraguirre.gbm.data.mapper.IPCStateToGraph2D;
import com.gbm.cesaraguirre.gbm.domain.entity.DataSetMaxMin;
import com.gbm.cesaraguirre.gbm.domain.entity.Graph2D;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphParameters;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphPoint;
import com.gbm.cesaraguirre.gbm.domain.interactors.AxisXLabelFormater;
import com.gbm.cesaraguirre.gbm.domain.interactors.AxisYLabelFormater;
import com.gbm.cesaraguirre.gbm.domain.interactors.GetLastIPCStatesUseCase;
import com.gbm.cesaraguirre.gbm.domain.utils.GraphPointUtils;
import com.gbm.cesaraguirre.gbm.presentation.view.entity.GraphFragmentViewEntity;


import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class IPCGraphViewModel extends ViewModel{

    private double ZOOM_X_DELTA_FACTOR = 100;
    private double ZOOM_Y_DELTA_FACTOR = 100;
    private double X_MOVMENT_DELTA_FACTOR = 1000;
    private double Y_MOVMENT_DELTA_FACTOR = 1000;
    private static final double INITIAL_WIDTH = 100;
    private static final double INITIAL_HEIGHT = 100;
    private static final double INITIAL_CENTER_X = 18000;
    private static final double INITIAL_CENTER_Y = 4000;

    private static final int POINT_TYPE_NORMAL = 1;
    private static final int POINT_TYPE_COLUMN = 2;
    private static final int POINT_TYPE_CONNECTED_LINE = 3;

    private double boxWidth = INITIAL_WIDTH;
    private double boxHeight = INITIAL_HEIGHT;
    private double boxCenterX = INITIAL_CENTER_X;
    private double boxCenterY = INITIAL_CENTER_Y;
    private Graph2D graphToDisplay;

    private double resolutionFactor = 10;
    private int gridTypeSet = GraphParameters.GRID_TYPE_X_AND_Y;
    private int point_type = POINT_TYPE_NORMAL;

    private GetLastIPCStatesUseCase getLastIPCStatesUseCase;
    private MutableLiveData<GraphFragmentViewEntity> graphFragmentViewEntityMutableLiveData;
    IPCStateToGraph2D pointMapper = new IPCStateToGraph2D();
    private boolean waitngServiceResponse = false;

    public void init(GetLastIPCStatesUseCase getLastIPCStatesUseCase) {
        this.getLastIPCStatesUseCase = getLastIPCStatesUseCase;
    }

    @SuppressLint("CheckResult")
    public LiveData<GraphFragmentViewEntity> getLiveDataForScreen() {
        if (graphFragmentViewEntityMutableLiveData == null) {
            graphFragmentViewEntityMutableLiveData = new MutableLiveData<>();

            getLastIPCStatesUseCase.execute()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> waitngServiceResponse = true)
                    .doFinally(() -> {
                        waitngServiceResponse = false;
                        postFreshLiveData();
                    })
                    .subscribe(ipcStates -> {
                        if (ipcStates.size()>0) {
                            List<GraphPoint> points = pointMapper.transform(ipcStates);
                            DataSetMaxMin maxMins = GraphPointUtils.getDataSetMaxMin(points);
                            boxWidth = maxMins.getMaxX() - maxMins.getMinX();
                            boxHeight = maxMins.getMaxY() - maxMins.getMinY();
                            boxCenterX = maxMins.getMaxX() - boxWidth / 2;
                            boxCenterY = maxMins.getMaxY() - boxHeight / 2;
                            graphToDisplay = new Graph2D(
                                    points,
                                    GraphParameters.X_SCALE_LINEAR,
                                    GraphParameters.Y_SCALE_LINEAR,
                                    GraphParameters.GRID_TYPE_X_AND_Y,
                                    GraphParameters.INTERPOLATION_TYPE_RECT_LINES,
                                    GraphParameters.POINT_TYPE_CIRCLE,
                                    Color.RED,
                                    Color.RED,
                                    Color.BLUE,
                                    Color.BLUE,
                                    Color.GRAY,
                                    1, 1,
                                    new AxisXLabelFormater(), new AxisYLabelFormater());
                            postFreshLiveData();
                        }
                    }, throwable -> {

                    });
        }
        return graphFragmentViewEntityMutableLiveData;
    }

    public void requestInitialState() {
        postFreshLiveData();
    }

    public void zoomXIn() {
        if (boxWidth - ZOOM_X_DELTA_FACTOR > 0) {
            boxWidth -= ZOOM_X_DELTA_FACTOR;
            postFreshLiveData();
        }
    }

    public void zoomXOut() {
        boxWidth += ZOOM_X_DELTA_FACTOR;
        postFreshLiveData();
    }

    public void zoomYIn() {
        if (boxHeight - ZOOM_Y_DELTA_FACTOR > 0) {
            boxHeight-= ZOOM_Y_DELTA_FACTOR;
            postFreshLiveData();
        }
    }

    public void zoomYOut() {
        boxHeight += ZOOM_Y_DELTA_FACTOR;
        postFreshLiveData();
    }

    public void moveLeft() {
        boxCenterX -= X_MOVMENT_DELTA_FACTOR;
        postFreshLiveData();
    }

    public void moveRight() {
        boxCenterX += X_MOVMENT_DELTA_FACTOR;
        postFreshLiveData();
    }

    public void moveUp() {
        boxCenterY += Y_MOVMENT_DELTA_FACTOR;
        postFreshLiveData();
    }

    public void moveDown() {
        boxCenterY -= Y_MOVMENT_DELTA_FACTOR;
        postFreshLiveData();
    }

    private void postFreshLiveData() {

        /*
        X_MOVMENT_DELTA_FACTOR = xResolution/3;
        Y_MOVMENT_DELTA_FACTOR = yResolution/3;
        */
        X_MOVMENT_DELTA_FACTOR = boxWidth / 20;
        Y_MOVMENT_DELTA_FACTOR = boxHeight / 20;
        ZOOM_X_DELTA_FACTOR = boxWidth /10;
        ZOOM_Y_DELTA_FACTOR = boxHeight/10;

        if (graphToDisplay != null) {
            double xResolution = boxWidth/resolutionFactor;
            double yResolution = boxHeight/resolutionFactor;
            graphToDisplay = Graph2D.clone(graphToDisplay)
                    .withGridXResolution(xResolution)
                    .withGridYResolution(yResolution)
                    .withGridType(gridTypeSet)
                    .withPointType(point_type==POINT_TYPE_COLUMN?GraphParameters.POINT_TYPE_BASE_POINT:GraphParameters.POINT_TYPE_CIRCLE)
                    .withInterpolationType(point_type == POINT_TYPE_CONNECTED_LINE ? GraphParameters.INTERPOLATION_TYPE_RECT_LINES : GraphParameters.INTERPOLATION_TYPE_NO_INTERPOLATION)
                    .build();
        }

        GraphFragmentViewEntity viewEntity = new GraphFragmentViewEntity(graphToDisplay,
                boxWidth, boxHeight, boxCenterX, boxCenterY, waitngServiceResponse);
        graphFragmentViewEntityMutableLiveData.postValue(viewEntity);
    }

    public void changeGridResolution() {
        resolutionFactor-=2;
        if (resolutionFactor <= 0) {
            resolutionFactor = 10;
        }
        postFreshLiveData();
    }

    public void changeGridMode() {
        if (gridTypeSet == GraphParameters.GRID_TYPE_NO_GRID) {
            gridTypeSet = GraphParameters.GRID_TYPE_X_AND_Y;
        }else if (gridTypeSet == GraphParameters.GRID_TYPE_X_AND_Y) {
            gridTypeSet = GraphParameters.GRID_TYPE_X;
        } else if (gridTypeSet == GraphParameters.GRID_TYPE_X) {
            gridTypeSet = GraphParameters.GRID_TYPE_Y;
        } else {
            gridTypeSet = GraphParameters.GRID_TYPE_NO_GRID;
        }
        postFreshLiveData();
    }

    public void changeGridDisplayType() {
        if (point_type == POINT_TYPE_NORMAL) {
            point_type = POINT_TYPE_COLUMN;
        } else if (point_type == POINT_TYPE_COLUMN){
            point_type = POINT_TYPE_CONNECTED_LINE;
        } else {
            point_type = POINT_TYPE_NORMAL;
        }
        postFreshLiveData();
    }
}
