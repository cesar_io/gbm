package com.gbm.cesaraguirre.gbm.data.repository;

import com.gbm.cesaraguirre.gbm.data.networking.IPCWebService;
import com.gbm.cesaraguirre.gbm.data.networking.WebServiceInstance;
import com.gbm.cesaraguirre.gbm.data.mapper.IPCWebServiceResponseMapper;
import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;
import com.gbm.cesaraguirre.gbm.domain.repository.IPCStateRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Function;


public class IPCStateRepositoryImpl implements IPCStateRepository{

    IPCWebService ipcWebService;
    final IPCWebServiceResponseMapper responseMapper;

    public IPCStateRepositoryImpl(IPCWebServiceResponseMapper responseMapper) {
        this.responseMapper = responseMapper;
        ipcWebService = WebServiceInstance.getRetrofitInstance().create(IPCWebService.class);
    }

    @Override
    public Single<List<IPCState>> getIPCStatesSorted() {
        return ipcWebService.getIPCStates()
                .map(ipcWebServiceResponse -> responseMapper.transform(ipcWebServiceResponse))
                .map((Function<List<IPCState>, List<IPCState>>) ipcStates -> {
                    ArrayList withoutNulls = new ArrayList();
                    for (IPCState ipcState : ipcStates) {
                        if (ipcState != null) {
                            withoutNulls.add(ipcState);
                        }
                    }
                    Collections.sort(withoutNulls);
                    return withoutNulls;
                });
    }
}
