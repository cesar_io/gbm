package com.gbm.cesaraguirre.gbm.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IPCWebServiceResponse {
    @SerializedName("result")
    boolean result;

    @SerializedName("msg")
    String message;

    @SerializedName("resultObj")
    List<WSIPCObject> resultObjList;

    public List<WSIPCObject> getResultObjList() {
        return resultObjList;
    }
}
