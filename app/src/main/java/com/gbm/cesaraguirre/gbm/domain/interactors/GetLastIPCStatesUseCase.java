package com.gbm.cesaraguirre.gbm.domain.interactors;

import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;
import com.gbm.cesaraguirre.gbm.domain.repository.IPCStateRepository;

import java.util.List;

import io.reactivex.Single;

public class GetLastIPCStatesUseCase extends ReactiveSingleUseCase<List<IPCState>>{

    private final IPCStateRepository repository;

    public GetLastIPCStatesUseCase(IPCStateRepository repository) {
        this.repository = repository;
    }

    @Override
    public Single<List<IPCState>> execute() {
        return repository.getIPCStatesSorted();
    }
}
