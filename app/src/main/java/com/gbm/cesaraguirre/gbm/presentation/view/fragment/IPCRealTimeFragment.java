package com.gbm.cesaraguirre.gbm.presentation.view.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gbm.cesaraguirre.gbm.R;
import com.gbm.cesaraguirre.gbm.data.mapper.IPCWebServiceResponseMapper;
import com.gbm.cesaraguirre.gbm.data.mapper.ResultObjResponseMapper;
import com.gbm.cesaraguirre.gbm.data.repository.IPCStateRepositoryImpl;
import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;
import com.gbm.cesaraguirre.gbm.domain.interactors.GetIPCStatesInRealTimeUseCase;
import com.gbm.cesaraguirre.gbm.presentation.view.adapter.IPCItemCallBack;
import com.gbm.cesaraguirre.gbm.presentation.view.adapter.IPCStatesListAdapter;
import com.gbm.cesaraguirre.gbm.presentation.viewmodel.IPCRealTimeViewModel;

import java.util.List;

public class IPCRealTimeFragment extends Fragment {

    RecyclerView ipcItemsRecyclerView;
    IPCStatesListAdapter ipcStatesAdapter;

    public IPCRealTimeFragment() {
        // Required empty public constructor
    }

    public static IPCRealTimeFragment newInstance() {
        IPCRealTimeFragment fragment = new IPCRealTimeFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IPCRealTimeViewModel viewModel = ViewModelProviders.of(this).get(IPCRealTimeViewModel.class);
        viewModel.init(new GetIPCStatesInRealTimeUseCase(new IPCStateRepositoryImpl(new IPCWebServiceResponseMapper(new ResultObjResponseMapper()))));
        viewModel.getLiveDataForIPCStates().observe(this, this::refreshUI);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_ipcreal_time, container, false);
        ipcItemsRecyclerView = v.findViewById(R.id.ipc_items_recycler);
        ipcItemsRecyclerView.setHasFixedSize(false);
        ipcItemsRecyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        ipcStatesAdapter = new IPCStatesListAdapter(new IPCItemCallBack());
        ipcItemsRecyclerView.setAdapter(ipcStatesAdapter);

        return v;
    }

    private void refreshUI(List<IPCState> newData) {
        ipcStatesAdapter.submitList(newData);
    }

}
