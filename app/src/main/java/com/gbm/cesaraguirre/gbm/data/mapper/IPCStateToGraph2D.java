package com.gbm.cesaraguirre.gbm.data.mapper;

import android.support.annotation.Nullable;

import com.gbm.cesaraguirre.gbm.domain.entity.GraphPoint;
import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;

import java.util.Calendar;
import java.util.Date;

public class IPCStateToGraph2D extends BaseDataMapper<GraphPoint, IPCState>{


    @Nullable
    @Override
    public GraphPoint transform(IPCState input) {
        if (input == null) {
            return null;
        }

        return new GraphPoint(input.getDate().getTime() , input.getPrice());
    }
}
