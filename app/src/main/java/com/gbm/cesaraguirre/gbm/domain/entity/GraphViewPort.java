package com.gbm.cesaraguirre.gbm.domain.entity;

import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;

public class GraphViewPort {

    private final RectF worldSectionToDisplay;
    private final Rect deviceRect;

    public GraphViewPort(RectF worldSectionToDisplay, Rect deviceRect){
        this.worldSectionToDisplay = worldSectionToDisplay;
        this.deviceRect = deviceRect;
    }

    //Map to surface coords
    public GraphPoint map(@NonNull GraphPoint naturalCoords) {
        return new GraphPoint(
                (float) map(naturalCoords.getX(), worldSectionToDisplay.left, worldSectionToDisplay.right, deviceRect.left, deviceRect.right),
                (float) map(naturalCoords.getY(), worldSectionToDisplay.bottom, worldSectionToDisplay.top, deviceRect.top, deviceRect.bottom)
                );
    }

    private double map(double value_x, double fromLow_a, double fromhight_b, double toLow_c, double toHight_d) {
        return (value_x-fromLow_a) / (fromhight_b-fromLow_a) * (toHight_d - toLow_c) + toLow_c;
    }

    public RectF getWorldSectionToDisplay() {
        return worldSectionToDisplay;
    }

    public Rect getDeviceRect() {
        return deviceRect;
    }
}
