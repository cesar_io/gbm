package com.gbm.cesaraguirre.gbm.data.networking;

import com.gbm.cesaraguirre.gbm.data.response.IPCWebServiceResponse;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface IPCWebService {

    @GET("/Mercados/ObtenerDatosGrafico?empresa=IPC")
    Single<IPCWebServiceResponse> getIPCStates();

}
