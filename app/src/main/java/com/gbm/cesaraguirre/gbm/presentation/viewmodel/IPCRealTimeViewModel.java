package com.gbm.cesaraguirre.gbm.presentation.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;
import com.gbm.cesaraguirre.gbm.domain.interactors.GetIPCStatesInRealTimeUseCase;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class IPCRealTimeViewModel extends ViewModel{

    GetIPCStatesInRealTimeUseCase getIPCStatesInRealTimeUseCase;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    static final int REFRESH_INTERVAL_IN_SECONDS = 5;
    static final int NUM_ITEMS_TO_DISPLAY = 15;

    public IPCRealTimeViewModel() { }

    public void init(GetIPCStatesInRealTimeUseCase getIPCStatesInRealTimeUseCase) {
        this.getIPCStatesInRealTimeUseCase = getIPCStatesInRealTimeUseCase;
    }

    private MutableLiveData<List<IPCState>> ipcStatesLD;

    public LiveData<List<IPCState>> getLiveDataForIPCStates() {
        if(ipcStatesLD == null) {
            ipcStatesLD = new MutableLiveData<>();

            Disposable disposable = getIPCStatesInRealTimeUseCase.execute(
                        new GetIPCStatesInRealTimeUseCase.ExecuteArgs(REFRESH_INTERVAL_IN_SECONDS, NUM_ITEMS_TO_DISPLAY))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(freshIPCStates -> {
                        ipcStatesLD.postValue(freshIPCStates);
                        Log.v("TAG", "Prices size: " + freshIPCStates.size());
                    }, throwable -> {

                    }, () -> {

                    });
            compositeDisposable.add(disposable);
        }
        return ipcStatesLD;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
