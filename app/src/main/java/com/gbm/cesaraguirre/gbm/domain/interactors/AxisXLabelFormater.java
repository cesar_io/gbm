package com.gbm.cesaraguirre.gbm.domain.interactors;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AxisXLabelFormater extends GraphLabelFormater {

    private final SimpleDateFormat sdf;

    @SuppressLint("SimpleDateFormat")
    public AxisXLabelFormater() {
        sdf = new SimpleDateFormat("HH:mm:ss.SSS");
    }

    @Override
    public String execute(double input) {
        long milisegs = (long) input;
        return sdf.format(new Date(milisegs));
    }
}
