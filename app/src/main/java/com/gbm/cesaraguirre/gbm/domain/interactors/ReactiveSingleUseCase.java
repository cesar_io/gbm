package com.gbm.cesaraguirre.gbm.domain.interactors;

import io.reactivex.Single;

public abstract class ReactiveSingleUseCase<T> {
    public abstract Single<T> execute();
}
