package com.gbm.cesaraguirre.gbm.presentation.view.entity;

import com.gbm.cesaraguirre.gbm.domain.entity.Graph2D;

public class GraphFragmentViewEntity {
    final Graph2D graphToDisplay;
    final double boxWidth;
    final double boxHeight;
    final double boxCenterX;
    final double boxCenterY;
    final boolean loading;


    public GraphFragmentViewEntity(Graph2D graphToDisplay, double boxWidth, double boxHeight, double boxCenterX, double boxCenterY, boolean loading) {
        this.graphToDisplay = graphToDisplay;
        this.boxWidth = boxWidth;
        this.boxHeight = boxHeight;
        this.boxCenterX = boxCenterX;
        this.boxCenterY = boxCenterY;
        this.loading = loading;
    }

    public Graph2D getGraphToDisplay() {
        return graphToDisplay;
    }

    public double getBoxWidth() {
        return boxWidth;
    }

    public double getBoxHeight() {
        return boxHeight;
    }

    public double getBoxCenterX() {
        return boxCenterX;
    }

    public double getBoxCenterY() {
        return boxCenterY;
    }

    public boolean isLoading() {
        return loading;
    }
}
