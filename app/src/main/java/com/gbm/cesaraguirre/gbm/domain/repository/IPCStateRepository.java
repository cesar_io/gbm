package com.gbm.cesaraguirre.gbm.domain.repository;

import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;

import java.util.Collection;
import java.util.List;

import io.reactivex.Single;

public interface IPCStateRepository {

    Single<List<IPCState>> getIPCStatesSorted();

}
