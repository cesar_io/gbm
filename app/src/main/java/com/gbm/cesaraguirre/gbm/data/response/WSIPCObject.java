package com.gbm.cesaraguirre.gbm.data.response;

import com.google.gson.annotations.SerializedName;

public class WSIPCObject {

    @SerializedName("Fecha")
    String fecha;

    @SerializedName("Precio")
    Double precio;

    @SerializedName("Porcentaje")
    Float porcentaje;

    @SerializedName("Volumen")
    Long volumen;

    public String getFecha() {
        return fecha;
    }

    public Double getPrecio() {
        return precio;
    }

    public Float getPorcentaje() {
        return porcentaje;
    }

    public Long getVolumen() {
        return volumen;
    }
}
