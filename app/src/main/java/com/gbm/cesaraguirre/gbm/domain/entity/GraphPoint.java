package com.gbm.cesaraguirre.gbm.domain.entity;

public class GraphPoint {
    private final double x;
    private final double y;

    public GraphPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
