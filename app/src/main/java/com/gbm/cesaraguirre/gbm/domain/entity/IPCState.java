package com.gbm.cesaraguirre.gbm.domain.entity;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.Objects;

public class IPCState implements Comparable<IPCState>{
    private final @NonNull Date date;
    private final @NonNull Double price;
    private final @NonNull Float percentage;
    private final @NonNull Long volume;

    public IPCState(@NonNull Date date, @NonNull Double price, @NonNull Float percentage, @NonNull Long volume) {
        this.date = date;
        this.price = price;
        this.percentage = percentage;
        this.volume = volume;
    }

    @NonNull
    public Date getDate() {
        return date;
    }

    @NonNull
    public Double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IPCState ipcState = (IPCState) o;
        return date.getTime() == ipcState.date.getTime();
    }

    @Override
    public int hashCode() {
        return Objects.hash(date.getTime());
    }

    @Override
    public int compareTo(@NonNull IPCState ipcState) {
        return this.getDate().compareTo(ipcState.date);
    }
}
