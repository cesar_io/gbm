package com.gbm.cesaraguirre.gbm.presentation.view.custom;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.gbm.cesaraguirre.gbm.domain.entity.Graph2D;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphParameters;
import com.gbm.cesaraguirre.gbm.domain.entity.GraphViewPort;
import com.gbm.cesaraguirre.gbm.presentation.view.entity.GraphFragmentViewEntity;

public class IPCGraphSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    Context context;
    boolean running = false;
    PlotRenderer plotRenderer;
    private Thread mRenderThread;

    public IPCGraphSurfaceView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        SurfaceHolder mSurfaceHolder = getHolder();
        mSurfaceHolder.addCallback(this);
    }

    public void resume() {
        startThread();
    }

    private void startThread() {
        if (plotRenderer != null) {
            running = true;
            mRenderThread = new Thread(plotRenderer);
            mRenderThread.start();
        }
    }

    public void pause() {
        running = false;
        try {
            mRenderThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        plotRenderer = new PlotRenderer(surfaceHolder);
        if (!running) {
            startThread();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        plotRenderer.refreshSurfaceMeasures(width, height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    public void refreshPlotModel(GraphFragmentViewEntity newModel) {
        if (plotRenderer!=null) {
            plotRenderer.setModelToRender(newModel);
        }
    }

}
