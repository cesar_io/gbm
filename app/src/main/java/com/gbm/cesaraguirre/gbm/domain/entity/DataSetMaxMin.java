package com.gbm.cesaraguirre.gbm.domain.entity;

public class DataSetMaxMin {
    private final double maxX;
    private final double minX;
    private final double maxY;
    private final double minY;

    public DataSetMaxMin(double maxX, double minX, double maxY, double minY) {
        this.maxX = maxX;
        this.minX = minX;
        this.maxY = maxY;
        this.minY = minY;
    }

    public double getMaxX() {
        return maxX;
    }

    public double getMinX() {
        return minX;
    }

    public double getMaxY() {
        return maxY;
    }

    public double getMinY() {
        return minY;
    }
}
