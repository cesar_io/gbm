package com.gbm.cesaraguirre.gbm.data.mapper;

import com.gbm.cesaraguirre.gbm.data.response.WSIPCObject;
import com.gbm.cesaraguirre.gbm.domain.entity.IPCState;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ResultObjResponseMapper extends BaseDataMapper<IPCState, WSIPCObject> {

    //2018-09-05T05:01:52.753-05:00
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
    private final SimpleDateFormat dateFormat;

    public ResultObjResponseMapper() {
        dateFormat = new SimpleDateFormat(DATE_FORMAT);
    }

    @Override
    public IPCState transform(WSIPCObject input) {
        IPCState ipcState;
        if (input == null ||
                input.getPrecio() == null ||
                input.getVolumen() == null ||
                input.getFecha() == null ||
                input.getPorcentaje() == null) {
            return null;
        }
        try {

            Date date = dateFormat.parse(input.getFecha());
            //System.out.println(dateFormat.format(date));
            ipcState = new IPCState(
                    date,
                    input.getPrecio(),
                    input.getPorcentaje(),
                    input.getVolumen()
            );
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return ipcState;
    }
}
